<nav id="nav">
    <ul class="links">
        <li <?php echo ($page === 'index' ? 'class="active"' : ''); ?>>
            <a href="index.php" title="Accueil">
                <span>Accueil</span>
            </a>
        </li>
        <li <?php echo ($page === 'articles' ? 'class="active"' : ''); ?>>
            <a href="articles.php" title="Articles">
                <span>Articles</span>
            </a>
        </li>
    </ul>

    <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
        <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
    </ul>
</nav>