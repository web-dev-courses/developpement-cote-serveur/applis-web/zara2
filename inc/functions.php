
<?php

// -------------------------------
// FONCTIONS UTILES
// -------------------------------

/**
 * Récupération du code hexadécimal d'une couleur à partir de son libellé
 */
function getCouleur($couleur) {
    switch ($couleur) {
        case 'noir':
            return '#000';
        case 'blanc':
            return '#FFF';
        case 'gris':
            return '#666';
        case 'marron':
            return '#744100';
        case 'jaune':
            return '#F5DC00';
        case 'rouge':
            return '#D41818';
        case 'bleu':
            return '#292CC9';
        case 'violet':
            return '#8929C9';
        case 'rose':
            return '#FF9CFC';
        default:
            return null;
    }
}

// -------------------------------
// REQUÊTES A LA BASE DE DONNEES
// -------------------------------

/**
 * Récupération des tailles d'un article à partir de l'id de l'article donné en paramètre
 */
function getTaillesByArticleId($mysqli, $idArticle)
{
    // Si l'id donné en paramètre n'est pas une entier c'est qu'il n'est pas valide,
    // on arrête donc l'exécution de la fonction en retournant NULL
    if (!is_int($idArticle)) {
        return null;
    }

    // Construction de la requête SQL
    $sql = '
        SELECT taille.intitule
        FROM article
        INNER JOIN article_has_taille
            ON article.id = article_has_taille.article_id
        INNER JOIN taille
            ON article_has_taille.taille_id = taille.id
        WHERE article.id = ' . $idArticle . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }

    // Récupération des résultats dans un tableau associatif
    $results = $query->fetch_all(MYSQLI_ASSOC);

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $results;
}

/**
 * Récupération des couleurs d'un article à partir de l'id de l'article donné en paramètre
 */
function getCouleursByArticleId($mysqli, $idArticle)
{
    // Si l'id donné en paramètre n'est pas une entier c'est qu'il n'est pas valide,
    // on arrête donc l'exécution de la fonction en retournant NULL
    if (!is_int($idArticle)) {
        return null;
    }

    // Construction de la requête SQL
    $sql = '
        SELECT couleur.intitule
        FROM article
        INNER JOIN article_has_couleur
            ON article.id = article_has_couleur.article_id
        INNER JOIN couleur
            ON article_has_couleur.couleur_id = couleur.id
        WHERE article.id = ' . $idArticle . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }

    // Récupération des résultats dans un tableau associatif
    $results = $query->fetch_all(MYSQLI_ASSOC);

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $results;
}

/**
 * Récupération des photos d'un article à partir de l'id de l'article donné en paramètre
 */
function getPhotosByArticleId($mysqli, $idArticle)
{
    // Si l'id donné en paramètre n'est pas une entier c'est qu'il n'est pas valide,
    // on arrête donc l'exécution de la fonction en retournant NULL
    if (!is_int($idArticle)) {
        return null;
    }

    // Construction de la requête SQL
    $sql = '
        SELECT photo.uri
        FROM article
        INNER JOIN photo
            ON article.id = photo.article_id
        WHERE article.id = ' . $idArticle . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }

    // Récupération des résultats dans un tableau associatif
    $results = $query->fetch_all(MYSQLI_ASSOC);

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $results;
}

/**
 * Récupération d'un article à partir de l'id donné en paramètre
 */
function getArticleById($mysqli, $id) {

    // Si l'id donné en paramètre n'est pas une entier c'est qu'il n'est pas valide,
    // on arrête donc l'exécution de la fonction en retournant NULL
    if (!is_int($id)) {
        return null;
    }

    // Construction de la requête SQL
    $sql = '
        SELECT article.reference, article.intitule, article.description,
            article.prix, article.genre,
            article_categorie.intitule as categorie
        FROM article
        INNER JOIN article_categorie
            ON article.article_categorie_id = article_categorie.id
        WHERE article.id = ' . $id . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }

    // Récupération des résultats dans un tableau associatif
    $result = $query->fetch_assoc();

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $result;
}

/**
 * Récupération des articles
 */
function getArticles($mysqli, $limit = null, $search = null, $sort = null) {
    // Construction de la requête SQL
    $sql = '
        SELECT article.id, article.intitule, article.description, article.prix,
               article_categorie.intitule AS c_intitule,
               MAX(photo.uri) AS photo
        FROM article
        INNER JOIN article_categorie
            ON article.article_categorie_id = article_categorie.id
        INNER JOIN photo
            ON article.id = photo.article_id
        ' . ($search !== null ? ' WHERE article.intitule LIKE "%' . $search . '%" ' : '') . '
        GROUP BY article.id, article.intitule, article.prix, article_categorie.intitule
        ORDER BY 
        ' . ($sort !== null ? ' article.intitule ' . $sort . ', ' : '') . '
        article.date_ajout DESC
        ' . ($limit !== null ? ' LIMIT ' . $limit : '') . '
        ;
    ';

    // Exécution de la requête SQL
    $query = $mysqli->query($sql);

    // Si aucune données n'a été trouvée en base de données on retourne NULL
    if (!$query) {
        return null;
    }
    
    // Récupération des résultats dans un tableau associatif
    $results = $query->fetch_all(MYSQLI_ASSOC);

    // Libération de l'espace mémoire réservé pour les résultats de la requête
    $query->free();

    return $results;
}
