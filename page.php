<?php
    include 'inc/head.php';

    if (!isset($page)) {
        echo 'La variable $page n\'est pas initialisée.';
        exit();
    }
?>

<body id="<?php echo $page; ?>" class="is-loading">
    <div id="wrapper" class="fade-in">

        <?php include 'inc/header.php'; ?>

        <div id="main">
            <div id="content">
                <?php include 'contents/content-' . $page . '.php'; ?>
            </div>
        </div>

        <?php include 'inc/footer.php'; ?>

    </div>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.scrollex.min.js"></script>
    <script src="assets/js/jquery.scrolly.min.js"></script>
    <script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/lib/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/lib/select2/select2.full.min.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>
</html>
